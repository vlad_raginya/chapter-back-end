$(document).ready(function(){
$('#login_form').submit(function(e) {
    e.preventDefault();
    console.log("!");
    $(".error").slideUp(300);
    var username=$("#username").val();
    var password=$("#password").val();
    if(username!='' && password!=''){
    $.ajax({
        type:"POST",
        url:"http://localhost:8080/login",
        data:{
            login:'login',
            username:username,
            password:password
        },
        success:function(response){
            console.log(response);
            if(response=="success")
            {
              window.location.href = "/PhotoApp/userpage.php";
            }
            else
            {
                $(".error").text("Invalid username or password");
                $(".error").slideDown(300);
                
            }
        }
    });
    }
    else
    {
        $(".error").text("All fields are required!");
        $(".error").slideDown(300);
        
    }
    return false;
});
$('#register_form').submit(function(e) {
    e.preventDefault();
    console.log("!");
    $(".error").slideUp(300);
    var username=$("#username").val();
    var password=$("#password").val();
    var fullname=$("#fullname").val();
    var email=$("#email").val();
    if(username!='' && password!='' && fullname!='' && email!=''){
    $.ajax({
        type:"POST",
        url:"register.php",
        data:{
            register:'register',
            username:username,
            password:password,
            fullname:fullname,
            email:email
        },
        success:function(response){
            console.log(response);
            $(".loading").hide();
            if(response=="success")
            {
                $(".error").text("Account successfully created");
                $(".error").slideDown(300);
            }
            else if(response=="exist")
            {
                $(".error").text("This username already exist. Choose other one");
                $(".error").slideDown(300);
                
            }
            else{
                $(".error").text("Failed to create an account");
                $(".error").slideDown(300);
            }
        }
    });
    }
    else
    {
        $(".error").text("All fields are required!");
        $(".error").slideDown(300);
        
    }
    return false;
});

$('#upload_avatar').submit(function(e) {
    e.preventDefault();
    $(".loading").show();
    console.log("!");
    var file_data = $('#avatar').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file',file_data);
    form_data.append('type',"avatar");
    console.log(form_data);
    $.ajax({
        type:"POST",
        url:"upload_photo.php",
        processData: false,
        cache: false,
        contentType: false,
        data: form_data,
        success:function(response){
            $(".loading").hide();
            console.log(response);
            d = new Date();
            if(response=="success"){
                $(".upload-avatar-container").hide();
                $(".user-bar,.logout-link,.user-photo").css("pointer-events","auto");
                $(".user-avatar").attr("src","img/avatar/"+usernameHash+".jpg?"+d.getTime());
                $('#avatar').val("");
                $(".drag-n-drop").text("Drag and drop your files here");
            }
            else if(response=="Extension"){
                alert("Wrong file extension");
            }
            else{
                alert("Failed to upload a file");
            }
        }
    });
    return false;
});

$('#upload_photo').submit(function(e) {
    e.preventDefault();
    $(".loading").show();
    console.log("buuba");
    var file_data = $('#photo').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file',file_data);
    form_data.append('type',"photo");
    console.log(file_data);
    for (var p of form_data) {
        console.log(p);
    }
    $.ajax({
        type:"POST",
        url:"http://localhost:8080/story/upload",
        processData: false,
        cache: false,
        //contentType: 'multipart/form-data',
        data: form_data,
        success:function(response){
            console.log(response);
            respArray = response.split(" ");
            if(respArray[0]=="success"){
                $(".upload-photo-container").hide();
                $(".user-bar,.logout-link,.user-photo").css("pointer-events","auto");
                $(".user-photos").append("<img class='user-photo' src='img/"+usernameHash+"/"+respArray[1]+"'>");
                $(".photos-container").load("userpage.php .photos-container");
                $('#photo').val("");
                $(".drag-n-drop").text("Drag and drop your files here");
            }
            else if(respArray[0]=="Extension"){
                alert("Wrong file extension");
            }
            else{
                alert("Failed to upload a file");
            }
        }
    });
    return false;
});


$(".change-avatar-btn").click(  function(){
    $(".upload-avatar-container").show();
    $(".user-bar,.logout-link,.user-photo").css("pointer-events","none");
});

$(".close-btn").click(  function(){
    $(".upload-avatar-container").hide();
    $(".user-bar,.logout-link,.user-photo").css("pointer-events","auto");
    $('#avatar').val("");
    $(".drag-n-drop").text("Drag and drop your files here");
});

$(".upload-photo-btn").click(  function(){
    $(".upload-photo-container").show();
    $(".user-bar,.logout-link,.user-photo").css("pointer-events","none");
});

$(".close-btn").click(  function(){
    $(".upload-photo-container").hide();
    $(".user-bar,.logout-link,.user-photo").css("pointer-events","auto");
    $('#photo').val("");
    $(".drag-n-drop").text("Drag and drop your files here");
});

$(".drag-n-drop").on("dragover",function(e){
    $(".drag-n-drop").addClass("hover");
    e.preventDefault();
});

$(".drag-n-drop").on("dragleave",function(e){
    $(".drag-n-drop").removeClass("hover");
    e.preventDefault();
});

$(".drag-n-drop").on("drop",function(e){
    e.preventDefault();
    $(".drag-n-drop").removeClass("hover");
    $('#photo,#avatar').prop('files', e.originalEvent.dataTransfer.files);
    let file = $('#photo,#avatar').val();
    file = file.split("\\");
    $(".drag-n-drop").text("You dropped "+file[2]+" Now press upload");
});

$(".user-photo").click(  function(){
    $(".opened-photo").css("background-image",$(this).css("background-image"));
    $(".opened-photo").show();
    $(".user-bar,.logout-link,.user-photo").css("pointer-events","none");
});

$(".close-btn").click(  function(){
    $(".opened-photo").hide();
    $(".user-bar,.logout-link,.user-photo").css("pointer-events","auto");
});

});