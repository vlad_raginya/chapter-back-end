FROM node:12

WORKDIR /usr

COPY package*.json ./

RUN npm install
COPY . .

EXPOSE 8080
CMD [ "node", "src/index.js" ]