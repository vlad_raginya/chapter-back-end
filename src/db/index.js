const { Pool } = require('pg');
const dbConfigLocal = require("../db/config.json")
const dbConfigHeroku = {
    connectionString: process.env.DATABASE_URL,
    ssl: {
        rejectUnauthorized: false
    }
}

if(process.env.DATABASE_URL) {
    module.exports = new Pool(dbConfigHeroku);
} else {
    module.exports = new Pool(dbConfigLocal);
}
