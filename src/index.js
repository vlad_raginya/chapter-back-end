const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const cors = require('cors')
const multer = require('multer')
const login = require('./routes/login')
const register = require('./routes/register')
const uploadPost = require('./routes/uploadPost')
const uploadStory = require('./routes/uploadStory')
const getAllPosts = require('./routes/getAllPosts')
const PORT = process.env.PORT || 8080

var upload = multer();
express()
  .use(cors())
  .use('/static', express.static('./public'))
  
  // .use(multer({
  //   uploadDir: path.join(__dirname, '../public/upload/temp')
  // }))
  // .use(bodyParser.urlencoded({extended : true}))
  // .use(bodyParser.json())
  .set('views', path.join(__dirname, 'views'))
  .get('/', (req, res) => res.send("Hello world of JavaScript"))
  .post('/login', login)
  .post('/register', register)
  .post("/story/upload", upload.single('photo'), uploadStory)
  .post("/post/upload", upload.array('photos'), uploadPost)
  .get('/post/getAll', getAllPosts)
  .listen(PORT, () => {
    console.log(`Example app listening at http://localhost:${PORT}`)
  })