const bcrypt = require('bcrypt')
const saltRounds = 10;
const pool = require('../db')

module.exports = async (req, res) => {
    const client = await pool.connect()
    let login = req.body.login
    let password = req.body.password
    console.log(login);
    console.log(password);
    let response = { 'message' : "" }
    client.query('SELECT * FROM "Users" WHERE username = $1;', [login], (err, result) => {
      console.log(err);
      if (result.rowCount > 0) {
        if (result.rows[0].login === login && bcrypt.compareSync(password,result.rows[0].password)) {
          response.message = "Success"
          res.send(JSON.stringify(response))
        }
        else {
          response.message = "Wrong password"
          res.send(JSON.stringify(response))
        }
      }
      else {
        response.message = "Wrong login"
        res.send(JSON.stringify(response))
      }
      client.release()
    })
}