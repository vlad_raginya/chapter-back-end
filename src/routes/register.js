const bcrypt = require('bcrypt')
const saltRounds = 10;
const pool = require('../db')

module.exports = async (req, res) => {
    const client = await pool.connect()
    //console.log(client)
    let login = req.body.username
    let password = req.body.password
    let email = req.body.email
    let fullname = req.body.fullname
    let response = { 'message' : "" }
    client.query('SELECT * FROM "Users" WHERE username = $1;', [login], (err, result) => {
        console.log(err);
      if (result.rowCount == 0) {
        let hashedPassword = bcrypt.hashSync(password,saltRounds)
        client.query('INSERT INTO "Users" (username,password,email,fullname) VALUES($1,$2,$3,$4);', [login,hashedPassword,email,fullname], (err,result) => {
            console.log(err);
            if (result) {
            response.message = "Success";
            res.send(JSON.stringify(response))
          }
        })
      }
      else {
        response.message = "User already exist"
        res.send(JSON.stringify(response))
      }
    })
    
    client.release()
}