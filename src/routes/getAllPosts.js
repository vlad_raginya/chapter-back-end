const bcrypt = require('bcrypt')
const saltRounds = 10;
const pool = require('../db')

module.exports = async (req, res) => {
    //console.log(process.env.DATABASE_URL)
    const client = await pool.connect()
    let response = { message : "", posts:[]}
    const result = await client.query('SELECT post_id, array_agg(story_id) FROM posts_stories GROUP BY post_id;');
    //console.log(result.rows);
    if (result.rowCount > 0) {
          for await (let row of result.rows) {
             //console.log(row)
             let post = {title:"", creation_date: "", photos: []};
             const resultPost = await client.query('SELECT * FROM posts WHERE id = $1;', [row.post_id]);
             //console.log(resultPost)
             if(resultPost.rowCount > 0) {
               post.creation_date = resultPost.rows[0].creation_date;
               post.title = resultPost.rows[0].name;
             }
             for await (let value of row.array_agg) {
                const resultStory = await client.query('SELECT * FROM stories WHERE id = $1;', [value]);
                //console.log(resultStory.rows)
                if(resultStory.rowCount > 0) {
                  post.photos.push(resultStory.rows[0].photo_path.replace("public", "static"));
                }
             }
             response.posts.push(post);
          }
          console.log("here")
          response.message = "Success";
          
      } else {
         response.message = "No posts"
      }
      
      res.send(JSON.stringify(response));
      client.release();
    
      //console.log(result.rows);
}