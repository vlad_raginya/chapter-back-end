const pool = require('../db')
const cloudUpload = require('../utils/cloudUpload')

module.exports = async (req, res) => {
    let uploadResult = await cloudUpload(req.file);
    //console.log(uploadResult);
    const client = await pool.connect()
   // console.log(req.body);
    //let data = req.body;
    let response = { 'message' : "" }
      client.query('INSERT INTO stories (photo_path) VALUES($1);', [uploadResult.secure_url], (err, result) => {
        if (result) {
          response.message = "Success";
          res.send(JSON.stringify(response))
        }
        client.release()
      })
    
}