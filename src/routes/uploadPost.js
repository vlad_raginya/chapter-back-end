const pool = require('../db')
const cloudUpload = require('../utils/cloudUpload')

module.exports = async (req, res) => {
  const client = await pool.connect()
  let response = { 'message' : "" }
  let files = req.files;
  let data = req.body;
  let postId;
  client.query('INSERT INTO posts (creation_date,name) VALUES($1,$2) RETURNING id', [Date.now().toString(), data.title],  (err, result) => {
    console.log(err)
    if (result) {
      postId = result.rows[0].id;
    }
  });

  for await (let file of files) {
    let uploadResult = await cloudUpload(file);
    //console.log(uploadResult);

    client.query('INSERT INTO stories (photo_path) VALUES($1) RETURNING *;', [uploadResult.secure_url], (err, result) => {
          //console.log(err)
      if (result) {
            //console.log(result);
        let storyId = result.rows[0].id;
            
                //console.log(result);
          client.query('INSERT INTO posts_stories (story_id, post_id) VALUES($1, $2);', [storyId, postId], (err, result) => {
              console.log(err)
              if (result) {
                    //console.log(result);
              }
          })
      }
    });
  }
 
  response.message = "Success";
  res.send(JSON.stringify(response))
  client.release()
}