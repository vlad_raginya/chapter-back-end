const cloudinary = require('cloudinary').v2
const streamifier = require('streamifier')

cloudinary.config({ 
    cloud_name: 'chapter-cloud', 
    api_key: '191999547372157', 
    api_secret: 'RkFUedzU4Zw9c-0VHJXU-YEbZUs' 
  });

module.exports = streamUpload = (file) => {
    return new Promise((resolve, reject) => {
        let stream = cloudinary.uploader.upload_stream(
          (error, result) => {
            if (result) {
              resolve(result);
            } else {
              reject(error);
            }
          }
        );

      streamifier.createReadStream(file.buffer).pipe(stream);
    });
};